import express from 'express';
import db from './db/db';
import bodyParser from 'body-parser';
import SoapCall from './soapRequest';

// Set up the express app
const app = express();

// Parse incoming requests data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// get all todos
app.get('/api/v1/todos', (req, res) => {
  res.status(200).send({
    success: 'true',
    message: 'todos retrieved successfully',
    todos: db
  })
});

app.get('/api/v1/soap', (req, res) => {
  
  let xmlhttp, envelope, url;

  url = 'http://sappi75-dev.gcba.gob.ar:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BS_DGREC&receiverParty=&receiverService=&interface=SI_I062_COORDENADAS_CAMPANA&interfaceNamespace=urn:dgrec:i062:coordenadas_campana';

  envelope = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">
                <soapenv:Header/>
                  <soapenv:Body>
                    <urn:ZFM_I062_COORDENADAS_CAMPANAS>
                      <!--Optional:-->
                      <READ>?</READ>
                    </urn:ZFM_I062_COORDENADAS_CAMPANAS>
                  </soapenv:Body>
                </soapenv:Envelope>`;
  

  SoapCall(url, envelope, (response) => {
    let items = response.Body["ZFM_I062_COORDENADAS_CAMPANAS.Response"].OT_COORDENADAS.item;
    return res.status(200).send({
      message: `Se encontraron ${items.length} items`,
      success: true,
      response: items,
    })       
  })
  
})

app.get('/api/v1/todo/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);
  const item = db.find(el => el.id === id);
  
  if(item) {
    return res.status(200).send({
      success: 'true',
      message: 'todo retrieved',
      todo: item
    })
  }
  else {
    return res.status(404).send({
      success: 'false',
      message: `No hay todo con id ${id}`,
    });
  }
})


//POST 
app.post('/api/v1/todos', (req, res) => {
  
  if(!req.body.title) 
  {
    return res.status(400).send({
      success: 'false',
      message: 'title is required'
    });
  } 
  else if(!req.body.description) 
  {
    return res.status(400).send({
      success: 'false',
      message: 'description is required'
    });
  }
  
 const todo = {
   id: db.length + 1,
   title: req.body.title,
   description: req.body.description
 }
 db.push(todo);
 return res.status(201).send({
   success: 'true',
   message: 'todo added successfully',
   todo
 })
});


const PORT = 5000;

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`)
});