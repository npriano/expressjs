import {XMLHttpRequest} from 'xmlhttprequest';
import {soap} from 'strong-soap';

const XMLHandler = new soap.XMLHandler();
const auth = 'Basic ' + Buffer.from('DFERNANDEZ:junio234').toString('base64');

function SoapCall(url, envelope, callback) {
  
  let xmlhttp, request, response;
  xmlhttp = new XMLHttpRequest();
  xmlhttp.open('POST', url, true);
  xmlhttp.setRequestHeader('Content-Type', 'text/xml');
  xmlhttp.setRequestHeader('Accept', 'text/xml');
  xmlhttp.setRequestHeader('Authorization', auth);
  
  xmlhttp.onreadystatechange = function () {
    if(xmlhttp.readyState == 4) {
      callback(XMLHandler.xmlToJson(null, xmlhttp.responseText))
    }
  };
  
  xmlhttp.send(envelope);
  
  return xmlhttp;
}

export default SoapCall;

